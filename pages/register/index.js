import  Router from 'next/router';
import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function index() {
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName , setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo , setMobileNo] = useState(0);
	const [isAdmin, setIsAdmin] = useState(false);

    function registerUser(e) {
		e.preventDefault();

		//fetch().then().then()
		fetch(`${ AppHelper.API_URL }/users/email-exists`, {
			method: 'POST',	
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email 
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === false){
				fetch(`${ AppHelper.API_URL }/users`,{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1
					})
				})
		
				.then(res => res.json())
				.then(data => {
					console.log(data)

						if(data){
							setFirstName('');
							setLastName('');
							setMobileNo(0);
							setEmail('');
							setPassword1('');
							setPassword2('');
								
							alert ('Thank you havve been registered')
							Router.push('/');
						}
				})
			}
		})
	}
	// form => registerUser()

	useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsAdmin(true);
		}else{
			setIsAdmin(false);
		}
			},[email, password1, password2]);

		return (
			<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>MobileNo</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter contact number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{isAdmin
					?
					<Button 
						className="bg-primary"
						type="submit"
						id="submitBtn"
					>
						Submit
					</Button>
					:
					<Button 
						className="bg-danger"
						type="submit"
						id="submitBtn"
						disabled
					>
						Submit
					</Button>
				}
		
			</Form>
		)
}